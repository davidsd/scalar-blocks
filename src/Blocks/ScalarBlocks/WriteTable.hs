{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Blocks.ScalarBlocks.WriteTable where

import           Blocks.ScalarBlocks.Types (BlockTableKey (..))
import           Data.Rendered                  (render)
import           System.FilePath                (takeDirectory, (<.>), (</>))
import           System.Process                 (callProcess)

blockTableFilePath :: FilePath -> BlockTableKey -> FilePath
blockTableFilePath blockTableDir BlockTableKey{..} =
  blockTableDir </> -- "nmax" ++ show nmax </>
  concat ((if output_ab then "abDerivTable" else "zzbDerivTable") : kvs)
  <.> "m"
  where
    kvs = [ "-" ++ k ++ v | (k, v) <- blockTableStrings ]
    blockTableStrings =
      [ ("d",             show dim)
      , ("delta12-",      render delta12)
      , ("delta34-",      render delta34)
      , ("L",             show spin)
      , ("nmax",          show nmax)
      , ("keptPoleOrder", show keptPoleOrder)
      , ("order",         show order)
      ]

writeBlockTableArgs :: Int -> FilePath -> BlockTableKey -> [String]
writeBlockTableArgs numThreads blockTableDir b@BlockTableKey{..} =
  [ "--dim",         show dim
  , "--delta-12",    render delta12
  , "--delta-34",    render delta34
  , "--spin-ranges", "0-" ++ show spin
  , "--max-derivs",  show nmax
  , "--poles",       show keptPoleOrder
  , "--order",       show order
  , "--precision",   show precision
  , "--num-threads", show numThreads
  , "-o",            takeDirectory (blockTableFilePath blockTableDir b)
  ] ++ (if output_ab then ["--output-ab"] else [])

-- | To make a table of blocks up to spin Lmax, we pass in the
-- BlockTable with spin = Lmax, since this will require making all the
-- lower-spin blocks as well. This is a bit of a hack.
writeBlockTable
  :: FilePath
  -> Int
  -> FilePath
  -> BlockTableKey
  -> IO ()
writeBlockTable scalarBlocksExecutable numThreads  blockTableDir b =
  callProcess scalarBlocksExecutable (writeBlockTableArgs numThreads blockTableDir b)

