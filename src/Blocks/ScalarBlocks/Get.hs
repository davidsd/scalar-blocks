{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Blocks.ScalarBlocks.Get where

import Blocks                        (Block (..), BlockBase, BlockFetchContext,
                                      BlockTableParams, ContinuumBlock (..),
                                      Coordinate (..), Delta (..),
                                      DerivMultiplier (..), HasBlocks,
                                      IsolatedBlock (..))
import Blocks                        qualified as B
import Blocks.Coordinate             (Derivative (..))
import Blocks.ScalarBlocks.ReadTable (DerivMap)
import Blocks.ScalarBlocks.Types     (FourRhoCrossing (..),
                                      ScalarBlockParams (..))
import Blocks.ScalarBlocks.Types     qualified as BT
import Bootstrap.Bounds              (ThreePointStructure (..))
import Bootstrap.Build               (Fetches (..))
import Bootstrap.Math.DampedRational (DampedRational)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.Polynomial     (Polynomial)
import Bootstrap.Math.Util           (choose, pochhammer)
import Bootstrap.Math.VectorSpace    ((*^), (^*))
import Control.DeepSeq               (NFData)
import Data.Binary                   (Binary)
import Data.Functor.Compose          (Compose (..))
import Data.Map.Strict               qualified as Map
import Data.Maybe                    (fromMaybe)
import Data.Proxy                    (Proxy (..))
import Data.Reflection               (reflect)
import Data.Rendered                 (mkRendered)
import Data.Tagged                   (Tagged)
import Data.Vector                   (Vector)
import Data.Vector                   qualified as V
import GHC.Generics                  (Generic)
import GHC.TypeNats                  (KnownNat, natVal)

data Standard3PtStruct d = Standard3PtStruct
  { operator1 :: ScalarRep d
  , operator2 :: ScalarRep d
  , operator3 :: SymTensorRep d
  }
  deriving (Eq, Ord, Show, Generic, Binary)

instance ThreePointStructure (Standard3PtStruct d) () (ScalarRep d) (SymTensorRep d) where
  makeStructure o1 o2 o3 _ = Standard3PtStruct o1 o2 o3

instance ThreePointStructure (Standard3PtStruct d) () (ScalarRep d) (ScalarRep d) where
  makeStructure o1 o2 o3 _ = Standard3PtStruct o1 o2 $ SymTensorRep (Fixed . scalarDelta $ o3) 0

data Standard4PtStruct = Standard4PtStruct
  deriving (Eq, Ord, Show, Generic, Binary)

data ScalarRep d = ScalarRep
  { scalarDelta :: Rational
  } deriving (Eq, Ord, Show, Generic, Binary)

data SymTensorRep d = SymTensorRep
  { symTensorDelta :: Delta
  , symTensorSpin  :: Int
  } deriving (Eq, Ord, Show, Generic, Binary)

newtype ScalarBlock d = ScalarBlock
  { unScalarBlock :: Block (Standard3PtStruct d) (Standard4PtStruct, DerivMultiplier)
  } deriving stock (Eq, Ord, Show, Generic)
    deriving newtype (Binary)

internalRep :: ScalarBlock d -> SymTensorRep d
internalRep b'@(ScalarBlock b) = if operator3 (struct12 b) == operator3 (struct43 b)
  then
    operator3 (struct12 b)
  else
    error $ "Mismatched internal operators in block " ++ show b'

type instance BlockFetchContext (ScalarBlock d) a m =
    Fetches BT.BlockTableKey (DR.DampedRational (FourRhoCrossing a) DerivMap a) m

type instance BlockTableParams (ScalarBlock d) = ScalarBlockParams

type instance BlockBase (ScalarBlock d) a = FourRhoCrossing a

instance (RealFloat a, NFData a, KnownNat d) => IsolatedBlock (ScalarBlock d) (Derivative 'ZZb) a where
  getBlockIsolated = getScalarBlockIsolated

instance (RealFloat a, NFData a, KnownNat d) => ContinuumBlock (ScalarBlock d) (Derivative 'ZZb) a where
  getBlockContinuum = getScalarBlockContinuum

-- Designed to match the function in Util.m
highPrecisionToString :: Rational -> String
highPrecisionToString x =
  case trim '0' .
       insertFromEnd digits '.' .
       (replicate (digits+1) '0' ++) .
       show $ i of
    "."         -> "0"
    s@('.' : _) -> ['-' | x < 0] ++ ('0' : s)
    s           -> ['-' | x < 0] ++ s
  where
    digits  = 32
    padding = 10
    i :: Integer = round (10^(digits+padding) * abs x) `quot` 10^padding
    trim c = reverse . dropWhile (== c) . reverse . dropWhile (== c)
    insertFromEnd n c s =
      let (a, b) = splitAt n (reverse s)
      in reverse (a ++ (c : b))

blockTableFromBlock :: forall d . KnownNat d => ScalarBlockParams -> ScalarBlock d -> BT.BlockTableKey
blockTableFromBlock params b'@(ScalarBlock b) = BT.BlockTableKey
  { dim           = fromIntegral (natVal @d Proxy)
  , delta12       = mkRendered highPrecisionToString delta12
  , delta34       = mkRendered highPrecisionToString delta34
  , spin          = symTensorSpin (internalRep b')
  , nmax          = nmax params
  , keptPoleOrder = keptPoleOrder params
  , order         = order params
  , precision     = precision params
  , output_ab     = False
  }
  where
    (delta12, delta34) = canonicalizeDelta12Delta34
      ( scalarDelta $ operator1 (struct12 b)
      , scalarDelta $ operator2 (struct12 b)
      , scalarDelta $ operator2 (struct43 b)
      , scalarDelta $ operator1 (struct43 b)
      )
    canonicalizeDelta12Delta34 (deltaI, deltaJ, deltaK, deltaL) =
      if deltaI > deltaJ
      then (deltaI - deltaJ, deltaK - deltaL)
      else (deltaL - deltaK, deltaJ - deltaI)

tableDelta :: Rational -> Int -> Rational
tableDelta dim l = fromIntegral l + dim - 2

unitarityBound :: Rational -> Int -> Rational
unitarityBound dim l =
  if l == 0
  then (dim - 2) / 2
  else fromIntegral l + dim - 2

fixedDelta :: forall d . KnownNat d => SymTensorRep d -> Rational
fixedDelta (SymTensorRep delta l) = case delta of
  Fixed delta'        -> delta'
  RelativeUnitarity x -> unitarityBound dim l + x
  where
    dim = fromIntegral (natVal @d Proxy)

shiftX :: forall d . KnownNat d => SymTensorRep d -> Rational
shiftX rep = fixedDelta rep - tableDelta dim (symTensorSpin rep)
  where
    dim = fromIntegral (natVal @d Proxy)

getScalarBlockRaw
  :: forall a m v p d . (HasBlocks (ScalarBlock d) p m a, Floating a, Eq a, Functor v, KnownNat d)
  => v (Derivative 'ZZb)
  -> ScalarBlock d
  -> Compose (Tagged p) m (DampedRational (FourRhoCrossing a) v a)
getScalarBlockRaw derivs b =
  Compose $ pure $
  convolveBlockTable b derivs <$>
  fetch (blockTableFromBlock (reflect @p Proxy) b)

addDeltaPhiSingleVar :: Floating a => a -> Int -> Vector a
addDeltaPhiSingleVar deltaPhi m' =
  V.fromList [ e * pochhammer (deltaPhi - fromIntegral k + 1) k
               -- Be careful to use Integer here to avoid overflow
               * fromIntegral (choose m k * 2^k :: Integer)
             | k <- [m, m-1 .. 0] ]
  where
    m :: Integer
    m = fromIntegral m'
    e = exp (-log 2 * deltaPhi)

addDeltaPhi :: (Floating a, Eq a) => a -> DerivMultiplier -> DerivMap (Polynomial a) -> Derivative 'ZZb -> Polynomial a
addDeltaPhi deltaPhi multiplier derivMap (Derivative (m, n)) =
  multiplier' *^ (Map.foldrWithKey' go 0 derivMap)
  where
    multiplier' = case multiplier of
      SChannel -> 1
      TChannel -> (-1)^(m+n)
    get v i = fromMaybe 0 (v V.!? i)
    u1 = addDeltaPhiSingleVar deltaPhi m
    u2 = addDeltaPhiSingleVar deltaPhi n
    go (Derivative (i, j)) p total =
      total + p ^* (get u1 i * get u2 j + if i == j then 0 else get u1 j * get u2 i)

unitBlockTable :: (Num a, Eq a) => Int -> DR.DampedRational (DR.One a) DerivMap a
unitBlockTable nmax_ = DR.constant $ Map.fromList $ do
  d <- V.toList (B.zzbDerivsAll nmax_)
  pure $ (d, if d == Derivative (0,0) then 1 else 0)

convolveBlockTable
  :: (Floating a, Eq a, Functor v)
  => ScalarBlock d
  -> v (Derivative 'ZZb)
  -> DampedRational base DerivMap a
  -> DampedRational base v a
convolveBlockTable (ScalarBlock b) derivVec derivMap = case (operator1 (struct12 b), operator2 (struct12 b)) of
  (ScalarRep delta1, ScalarRep delta2) ->
    let deltaPhi = fromRational (-(delta1 + delta2)/2)
        multiplier = snd . fourPtFunctional $ b
    in DR.mapNumerator (\num -> fmap (addDeltaPhi deltaPhi multiplier num) derivVec) derivMap

getScalarBlockContinuum
  :: forall d p m a v . (HasBlocks (ScalarBlock d) p m a, Floating a, Eq a, Functor v, KnownNat d)
  => v (Derivative 'ZZb)
  -> ScalarBlock d
  -> Compose (Tagged p) m (DampedRational (FourRhoCrossing a) v a)
getScalarBlockContinuum derivs b =
  DR.shift dx <$> getScalarBlockRaw derivs b
  where
    dx = shiftX (internalRep b)

getScalarBlockIsolated
  :: forall d p m a v . (HasBlocks (ScalarBlock d) p m a, RealFloat a, Functor v, Foldable v, KnownNat d)
  => v (Derivative 'ZZb)
  -> ScalarBlock d
  -> Compose (Tagged p) m (v a)
getScalarBlockIsolated derivs b =
  if symTensorSpin rep == 0 && delta == 0
  then
    pure $ DR.evalCancelPoleZeros 0 $ convolveBlockTable b derivs (unitBlockTable (nmax params))
  else
    DR.evalCancelPoleZeros (fromRational (shiftX rep)) <$> getScalarBlockRaw derivs b
  where
    rep = internalRep b
    params = reflect @p Proxy
    delta = fixedDelta rep
