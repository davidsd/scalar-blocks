{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeOperators         #-}

module Blocks.ScalarBlocks.Types where

import           Data.Aeson      (FromJSON, ToJSON)
import           Data.Binary     (Binary)
import           Data.Reflection (Reifies (..))
import           Data.Rendered   (Rendered)
import           GHC.Generics    (Generic)
import           Hyperion.Static (Dict (..), Static (..), cPtr)

data BlockTableKey = BlockTableKey
  { dim           :: Int
  , delta12       :: Rendered Rational
  , delta34       :: Rendered Rational
  , spin          :: Int
  , nmax          :: Int
  , keptPoleOrder :: Int
  , order         :: Int
  , precision     :: Int
  , output_ab     :: Bool
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance Static (Binary BlockTableKey) where closureDict = cPtr (static Dict)

data ScalarBlockParams = ScalarBlockParams
  { nmax          :: Int
  , keptPoleOrder :: Int
  , order         :: Int
  , precision     :: Int
  } deriving (Show, Eq, Ord, Generic, Binary, FromJSON, ToJSON)

data FourRhoCrossing a = FourRhoCrossing

instance Floating a => Reifies (FourRhoCrossing a) a where
  reflect _ = 4*(3 - 2*sqrt 2)

