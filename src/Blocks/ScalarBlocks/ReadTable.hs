{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}

module Blocks.ScalarBlocks.ReadTable where

import System.IO (hFlush, stdout)
import           Control.Applicative                 ((<|>))
import           Control.Monad                       (void)
import           Data.Attoparsec.ByteString.Char8    (Parser)
import qualified Data.Attoparsec.ByteString.Char8    as A
import qualified Data.ByteString.Char8               as BS
import           Data.Functor.Identity               (Identity (..))
import           Data.Map.Strict                     (Map)
import qualified Data.Map.Strict                     as Map
import qualified Data.MultiSet                       as MultiSet
import           Data.Rendered                       (value)
import qualified Data.Scientific                     as S
import qualified Data.Vector                         as V
import qualified Data.Vector.Mutable                 as MV
import           Blocks.ScalarBlocks.Types      (BlockTableKey (..),
                                                      FourRhoCrossing)
import           Blocks.ScalarBlocks.WriteTable (blockTableFilePath)
import qualified Bootstrap.Math.DampedRational            as DR
import           Bootstrap.Math.Polynomial                (Polynomial)
import qualified Bootstrap.Math.Polynomial                as Pol
import Blocks.Coordinate (Derivative(..), Coordinate(..))

type DerivMap = Map (Derivative 'ZZb)

ignore :: Parser a -> Parser ()
ignore p = void p <|> pure ()

spaced :: Parser a -> Parser a
spaced p = A.skipSpace *> p <* A.skipSpace

wolframNumber :: RealFloat a => Parser a
wolframNumber = do
  s <- A.scientific
  ignore ("`" *> A.scientific)
  e <- A.option 0 ("*^" *> A.signed A.decimal)
  return (S.toRealFloat (s * S.scientific 1 e))

polynomial :: RealFloat a => Parser (Polynomial a)
polynomial = fmap (Pol.fromVector . termListToVector) termList
  where
    monomial    = (,) <$> coefficient <* spaced (ignore "*") <*> expo
    coefficient = A.option 1 wolframNumber
    expo        = A.option 0 ("x" *> A.option 1 ("^" *> A.decimal))
    sign        = spaced ("+" *> pure 1 <|> "-" *> pure (-1))
    termList    = (:) <$> monomial <*> A.many' ((\s (a, n) -> (s*a, n)) <$> sign <*> monomial)
    termListToVector terms = V.create $ do
      let deg = snd (last terms)
      v <- MV.replicate (deg+1) 0
      mapM_ (\(a,n) -> MV.unsafeWrite v n a) terms
      return v

derivMap :: RealFloat a => Parser (DerivMap (Polynomial a))
derivMap = ignore comment *> "{" *> fmap Map.fromList (mapPair `A.sepBy` spaced ",") <* "}"
  where
    comment = spaced ("(*" *> A.manyTill A.anyChar "*)")
    deriv = Derivative <$> ((,) <$> ("zzbDeriv[" *> A.decimal <* spaced ",") <*> A.decimal <* "]")
    mapPair = (,) <$> (deriv <* spaced "->") <*> polynomial

readDerivMapFile :: RealFloat a => FilePath -> IO (Either String (DerivMap (Polynomial a)))
readDerivMapFile file = A.parseOnly derivMap <$> BS.readFile file

cbPoles :: Rational -> Int -> Int -> [Rational]
cbPoles nu spin keptPoleOrder =
  [ -(n + l - 1) | n <- [1 .. o] ] ++
  [ -(-nu + k - 1) | k <- [1 .. o / 2] ] ++
  [ -(-l - 2*nu + n - 1) | n <- [1 .. minimum [o, l]] ]
  where l = fromIntegral spin
        o = fromIntegral keptPoleOrder

-- | The damped rational (4 rho_c)^Delta / \prod_i (Delta - Delta_i),
-- shifted by writing Delta -> x + l + dim - 2.
cbPrefactor
  :: (Floating a, Eq a)
  => Int
  -> Int
  -> Int
  -> DR.DampedRational (FourRhoCrossing a) Identity a
cbPrefactor dim l keptPoleOrder =
  DR.shift (fromIntegral (l + dim - 2)) $
  DR.DampedRational (Identity 1) (MultiSet.fromList poles)
  where
    poles = cbPoles nu l keptPoleOrder
    nu    = (toRational dim - 2)/2

-- | Multiply the numerator in cbPrefactor by the polynomials obtained
-- from the block table files. Given a polynomial P(x) the resulting
-- function is:
-- 
-- (4 rho_c)^(x + l + dim - 2) P(x) / prod_i (x + l + dim - 2 - Delta_i).
--
-- Thus, although scalar_blocks uses the normalization convention
-- (rho_c)^Delta, the result returned by readBlockTable uses the
-- convention (4 rho_c)^Delta. Specifically, according to the table of
-- normalizations in the numerical bootstrap review
-- 'https://arxiv.org/pdf/1805.04405.pdf', scalar_blocks uses the
-- noramlization in line 5, whereas this library uses the
-- normalization in line 2.
--
-- Note that in the normalization used here, scalar OPE coefficients
-- are permutation symmetric.
readBlockTable
  :: RealFloat a
  => FilePath
  -> BlockTableKey
  -> IO (DR.DampedRational (FourRhoCrossing a) DerivMap a)
readBlockTable blockTableDir b@BlockTableKey{..} = do
  let file = blockTableFilePath blockTableDir b
  putStrLn $ "Reading BlockTable: " ++ file
  hFlush stdout
  m <- fmap (either error id) (readDerivMapFile file)
  let dr = DR.mapNumerator (\(Identity n) -> fmap (n*) m) $
           cbPrefactor dim spin keptPoleOrder
  return $
    -- In this case, the block tables have a pole and compensating
    -- zero at the unitarity bound. We must remove them in order to be
    -- able to evaluate the blocks there.
    if spin /= 0 && (value delta12 == 0 || value delta34 == 0)
    then DR.cancelPoleZeros 0 dr
    else dr
