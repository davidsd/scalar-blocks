scalar-blocks
---------------

A library for computing scalar conformal blocks using the C++ program
[scalar_blocks](https://gitlab.com/bootstrapcollaboration/scalar_blocks).

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/scalar-blocks/)
